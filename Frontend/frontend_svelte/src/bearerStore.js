import {writable} from "svelte/store";

const storedToken = ""
export const bearerToken = writable(storedToken)
if (window && localStorage.getItem("bearerToken") !== null) {
    if (localStorage.getItem("bearerToken").length !== 0) {
        let request = new XMLHttpRequest();
        request.addEventListener("load", () => {
            if (request.status === 401) {
                bearerToken.set("")
            }
        });
        request.open("GET", "http://localhost:8000/auth");
        request.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("bearerToken"));
        request.send();
    }
    bearerToken.set(localStorage.getItem("bearerToken"))
}
bearerToken.subscribe(value => {
    if (value != null && value !== "null" && value !== undefined) {
        localStorage.setItem("bearerToken", value);
    }
});