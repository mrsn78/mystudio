export class Setup {
    id = 0;
    name = "";
    components = [];

    constructor(id) {
        this.id = id;
    }
}