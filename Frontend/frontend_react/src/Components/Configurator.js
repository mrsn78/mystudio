import React from "react";
import Elements from "./Elements";
import Timeline from "./Timeline";
import Sidebar from "./Sidebar";
import '../Styles/Configurator.css'
import {useLocation, useNavigate} from "react-router-dom";

const HelperButtonClass = (props) => {
    let navigate = useNavigate()
    let goToCheckout = () => {
        navigate('/checkout',{state: {itemData}} ,{replace: true})
    }
    let itemData
    return (
        <button onClick={() => {
            itemData = props.itemGetter();
            localStorage.setItem('itemData', JSON.stringify(itemData))
            console.log(itemData)
            goToCheckout();
        }}>Auschecken</button>
    )
}

class Configurator extends React.Component {

    constructor(props) {
        super(props);

        this.sidebar = React.createRef()
        this.elements = React.createRef()

        this.state = {
            numberSelected: 0,
            itemBoolean: false
        }
        this.handleChangeSelected = this.handleChangeSelected.bind(this)
    }

    changeStatusBoolean = (boolean) => {
        this.setState({
            itemBoolean: boolean
        })
    }

    testingChild = (data) => {
        console.log(data)
    }

    handleSidebar = (bool) => {
        this.sidebar.current.showSidebar(bool)
    }

    colorItems = (number) => {
        this.elements.current.colorItems(number)
    }

    getItemData = () => {
        return this.sidebar.current.getAllSelectedItems()
    }

    getCategory = () => {
        return this.elements.current.getCategory()
    }

    handleChangeSelected(number) {
        this.setState(prevState => {
            if (prevState.numberSelected + number > 2 || prevState.numberSelected + number < 0) {
                return {numberSelected: prevState.numberSelected}
            }
            return (
                {numberSelected: prevState.numberSelected + number}
            )
        })
    }

    render() {
        return (
            <div className={'configuratorContainer'}>
                <div className={'timelineAndElements'}>
                    <div className={'elements'}>
                        <Elements ref={this.elements} selectElements={this.handleChangeSelected}
                                  numberElements={this.state.numberSelected} sidebarToggler={this.handleSidebar}/>
                    </div>
                    <div className={'timelineContainer'}>
                        <Timeline/>
                        {<div className={this.state.itemBoolean ? 'checkoutButton' : 'checkoutButton active'}>
                            <HelperButtonClass itemGetter={this.getItemData}/></div>}
                    </div>
                </div>
                <div className={'sidebarDiv'}>
                    <Sidebar ref={this.sidebar} showCart={this.props.showCart} itemColorer={this.colorItems}
                             getter={this.getCategory}
                             changer={this.changeStatusBoolean} logger={this.testingChild}
                             itemSetter={this.props.itemSetter} itemDataApp={this.props.itemData}
                             deleteItemData={this.props.deleteItemData}/>
                </div>
            </div>
        )
    }
}

export default Configurator