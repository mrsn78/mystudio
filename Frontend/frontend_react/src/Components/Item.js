import React from "react";
import '../Styles/item.css'

class Item extends React.Component {

    render() {
        return (
            <div onClick={this.props.onClick} className={this.props.class}>
                <p>{this.props.name}</p>
                <p>{this.props.price}</p>
            </div>
        )
    }
}

export default Item