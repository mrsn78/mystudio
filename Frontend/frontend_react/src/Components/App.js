import React, {useState} from "react";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import Login from '../Components/Login'
import Start from '../Components/Start'
import Register from '../Components/Register'
import Checkout from '../Components/Checkout'
import Configurator from "./Configurator";
import Image from '../Assets/Logo.svg'
import '../Styles/GeneralStyle.css'
import UserDisplay from "./UserDisplay";
import Cookies from "universal-cookie";
import Setups from "./Setups";
import CartList from "./CartList";

const cookies = new Cookies();

function App() {

    const [user, setUser] = useState(checkLogin());
    const [loggedIn, setLoggedIn] = useState(user !== undefined);
    const [items, setItems] = useState([])

    let changeLogged = (status) => {
        setLoggedIn(status);
        setUser(checkLogin());
    }
    let setItemData = (data) => {
        for (let i = 0; i < items.length; i++) {
            if (items[i].category === data.category) {
                if (items[i].id !== data.id) {
                    items[i] = data
                    setItems(items)
                }
                return
            }
        }
        setItems(oldArray => [...oldArray, data])
    }
    let deleteItemData = (index) => {
        setItems(oldArray => oldArray.filter((items, i) => i !== index));
    }
    let resetItemData = () => {
        setItems([])
    }

    return (
        <Router>
            <div className={'headerImage'}>
                <CartList className={items.length > 0 ? 'cartListDiv active' : 'cartListDiv'} itemData={items}/>
                <img onClick={() => {
                    window.location = "http://localhost:3000/";
                    localStorage.clear()
                }} className={'Logo'} src={Image} alt={'This is a Logo'}/>
                <UserDisplay logFunction={changeLogged} loggedStatus={loggedIn} user={user}/>
            </div>
            <Routes>
                <Route path={'/'} element={<Start loggedStatus={loggedIn}/>}/>
                <Route path={'/login'} element={<Login logFunction={changeLogged}/>}/>
                <Route path={'/register'} element={<Register logFunction={changeLogged}/>}/>
                <Route path={'/configurator'}
                       element={<Configurator itemSetter={setItemData} itemData={items}
                                              deleteItemData={deleteItemData}/>}/>
                <Route path={'/checkout'} element={<Checkout loggedStatus={loggedIn} logFunction={changeLogged}
                                                             resetter={resetItemData}/>}/>
                <Route path={'/setups'}
                       element={<Setups loggedStatus={loggedIn} itemsSetter={setItems} logFunction={changeLogged}/>}/>
            </Routes>
        </Router>
    );
}

function checkLogin() {
    let token = cookies.get("bearer_token")
    if (token === undefined) {
        return undefined;
    }

    let user = getUserFromBackend(token);
    if (user === undefined) {
        cookies.remove("bearer_token");
        return undefined;
    } else {
        return user;
    }
}

function getUserFromBackend(token) {
    let request = new XMLHttpRequest()
    request.open('GET', "http://localhost:8000/auth", false);
    request.setRequestHeader("Authorization", "Bearer " + token);

    try {
        request.send();
    } catch (e) {
        return undefined
    }
    let response = request.responseText

    if (response.startsWith("{")) {
        return JSON.parse(response)["username"];
    } else {
        return undefined;
    }
}

export default App;
