import React, {useState} from "react";
import {useNavigate} from "react-router-dom";
import Icon from "../Assets/account_circle_black_24dp.svg"
import "../Styles/UserDisplay.css"
import Cookies from "universal-cookie";

const cookies = new Cookies();

function UserDisplay(props) {
    let navigate = useNavigate();

    let iconClicked = (event) => {
        let dropDown = event.target.parentElement.lastElementChild;
        if(dropDown.classList.contains("disabled")) {
            dropDown.classList.remove("disabled");
            dropDown.classList.add("enabled");
        } else {
            dropDown.classList.remove("enabled");
            dropDown.classList.add("disabled");
        }
    }

    let goToSetups = (event) => {
        let dropDown = event.target.parentElement;
        dropDown.classList.remove("enabled");
        dropDown.classList.add("disabled");
        navigate("/setups", {replace: true});
    }

    let logOut = (event) => {
        let request = new XMLHttpRequest()
        request.open('POST', "http://localhost:8000/logout?username=" + props.user);
        request.send();
        cookies.remove("bearer_token");
        let dropDown = document.getElementById("userDropdown");
        dropDown.classList.remove("enabled");
        dropDown.classList.add("disabled");
        props.logFunction(false);
    }

    return (
        <div id={"userDiv"}>
            <img id={"userIcon"} src={Icon} alt={"UserIcon"} onClick={iconClicked} hidden={!props.loggedStatus}/>
            <div id={"userDropdown"} className={"disabled"}>
                <div id={"username"}>{props.user}</div>
                <button id={"savedSetupsBtn"} onClick={goToSetups}>Gespeicherte Setups</button>
                <button id={"logoutBtn"} onClick={logOut}>Ausloggen</button>
            </div>
        </div>
    );
}

export default UserDisplay