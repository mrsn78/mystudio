import React from "react";
import '../Styles/Checkout.css';
import {useLocation, useNavigate} from "react-router-dom";
import ListItem from "./ListItem";
import Cookies from "universal-cookie";

const cookies = new Cookies();

function Checkout(props){

    const navigate = useNavigate()
    let goBack = () =>{
        navigate('/configurator', {state: {itemData}},{replace: true})
    }

    let finish = () =>{
        localStorage.clear()
        props.resetter()
        navigate('/', {replace: true})
    }

    let save = (event) => {
        event.target.hidden = true;

        let body = '{"components": [';
        itemData.forEach((item) => {
            body += item[0].id + ",";
        });

        body = body.substring(0,body.length-1);

        body += "]}";

        console.log(body);

        let request = new XMLHttpRequest()
        request.open('POST', "http://localhost:8000/setup/save", false);
        request.setRequestHeader("Authorization", "Bearer " + cookies.get("bearer_token"));
        request.setRequestHeader("Content-Type", "application/json");

        request.addEventListener("load", function () {
            let response = request.responseText
            console.log("Save Response: " + response)
            console.log(request.status)

            if(request.status === 401){
                props.logFunction(false);
            }
        });

        request.send(body);
    }

    const {state} = useLocation()
    const itemData = state.itemData
    let itemList = []
    let sum = 0

    itemData.forEach((item,index) => {
        let listItem = item[0]
        itemList.push(<ListItem number={index+1} category={listItem.type.charAt(0).toUpperCase() + listItem.type.slice(1)} name={listItem.name} preis={listItem.price} link={listItem.link} />)
        sum += parseFloat(listItem.price)
    })


        return(
            <div className={'checkoutContainer'}>
                <div id={"checkoutHead"}>
                    <h1 id={'checkoutH1'} onClick={() => console.log(itemData)}>Auswahl</h1>
                    <button className={"checkoutTaskButton"} id={"saveButton"} onClick={save} hidden={!props.loggedStatus}>Setup speichern</button>
                </div>
                {itemList}
                <p>Summe: {sum}$</p>
                <div className={'buttonGroup'}>
                    <button onClick={goBack} className={'checkoutTaskButton'} id={"checkoutBackButton"}>Zurück</button>
                    <button onClick={finish} className={'checkoutTaskButton'} id={"checkoutFinishButton"}>Fertig</button>
                </div>
            </div>
        )

}

export default Checkout