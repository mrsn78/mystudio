import '../Styles/Start.css'
import '../Styles/GeneralStyle.css'
import React from "react";
import {useNavigate} from "react-router-dom";


function Start(props) {
    let navigate = useNavigate()
    let goToLogin = () => {
        navigate('/login', {replace: true})
    }
    let goToConfigurator = () => {
        navigate('configurator', {replace: true})
    }
    return (
        <div className={'StartContainer'}>
            <button onClick={goToLogin} hidden={props.loggedStatus}>Zum Login</button>
            <button className={'startButton'} onClick={goToConfigurator}>Konfiguration starten</button>
        </div>
    )
}

export default Start