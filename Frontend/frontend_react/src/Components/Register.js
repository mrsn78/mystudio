import '../Styles/Register.css'
import '../Styles/GeneralStyle.css'
import {useNavigate} from "react-router-dom";
import Cookies from "universal-cookie";
import React, {useState} from "react";

function Register(props) {
    const cookies = new Cookies();
    let navigate = useNavigate()
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");

    let goToLogin = () => {
        navigate('/login', {replace: true})
    }
    let goBack = () => {
        navigate('/', {replace: true})
    }

    let goToConfigurator = () => {
        navigate('/configurator', {replace: true})
    }

    function handleInputChange(event) {
        if (event.target.id == "username") {
            setUsername(event.target.value);
        } else if (event.target.id == "password") {
            setPassword(event.target.value);
        } else if (event.target.id == "email") {
            setEmail(event.target.value);
        }
    }

    function register() {
        let request = new XMLHttpRequest();

        request.addEventListener("load", () => {
            let response = request.responseText;
            if (response.startsWith("{")) { //Test if Json
                response = JSON.parse(response);
                cookies.set("bearer_token", response["bearer_token"], {path: "/"});
                props.logFunction(true);
                goToConfigurator();
            } else {
                document.getElementById("errorTxt").innerText = response;
            }
        });
        let url = "http://localhost:8000/register?";
        url += "username=" + username;
        url += "&password=" + password;
        url += "&email=" + email;
        request.open("POST", url);
        request.send();
    }

    return (
        <div className={'RegisterContainer'}>
            <h1>Register</h1>
            <form className={'RegisterForm'}>
                <div className={'inputField'}>
                    <label htmlFor={'username'}>Nutzername</label>
                    <input id={'username'} type={'text'} className={'textInput'} value={username}
                           onChange={handleInputChange}/>
                </div>
                <div className={'inputField'}>
                    <label htmlFor={'email'}>E-Mail</label>
                    <input id={'email'} type={'email'} className={'textInput'} value={email}
                           onChange={handleInputChange}/>
                </div>
                <div className={'inputField'}>
                    <label htmlFor={'password'}>Passwort</label>
                    <input id={'password'} type={'password'} className={'textInput'} value={password}
                           onChange={handleInputChange}/>
                </div>
                <div id={"errorTxt"}>
                </div>
            </form>
            <button onClick={register}>Registrieren</button>
            <div>
                <button onClick={goBack}>Zurück</button>
                <button onClick={goToLogin}>Login</button>
            </div>
        </div>
    )
}

export default Register