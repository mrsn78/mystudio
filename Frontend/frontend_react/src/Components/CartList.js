import '../Styles/CartList.css'
import Icon from "../Assets/shopping_basket_black_24dp.svg"
import {useState} from "react";
import anime from "animejs";

function CartList(props) {
    const [listToggled, setListToggled] = useState(true)

    let buildList = (array) => {
        let itemList = []
        array.forEach(element => {
            itemList.push(<li>{element.name}</li>)
        })
        return itemList
    }
    return (
        <div className={props.className}>
            <img src={Icon} id={'cartIcon'} onClick={() => {
                setListToggled(prevState => !prevState)
            }}/>
            <div className={listToggled ? 'listContainer' : 'listContainer active'}>
                <ol>
                    {buildList(props.itemData)}
                </ol>
            </div>
        </div>
    )
}

export default CartList