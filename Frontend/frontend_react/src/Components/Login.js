import '../Styles/Login.css'
import '../Styles/GeneralStyle.css'
import {useNavigate} from "react-router-dom";
import React, {useState} from "react";
import Cookies from "universal-cookie";

function Login(props) {
    const cookies = new Cookies();
    const navigate = useNavigate();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    let goToRegister = () => {
        navigate('/register', {replace: true})
    }
    let goBack = () => {
        navigate('/', {replace: true})
    }

    let goToConfigurator = () => {
        navigate('/configurator', {replace: true})
    }

    function handleInputChange(event) {
        if (event.target.id == "username") {
            setUsername(event.target.value);
        } else if (event.target.id == "password") {
            setPassword(event.target.value);
        }
    }

    function login() {
        let request = new XMLHttpRequest();

        request.addEventListener("load", () => {
            let response = request.responseText;
            if (response.startsWith("{")) { //Test if Json
                response = JSON.parse(response);
                if (response.hasOwnProperty("error")) { //Check for error
                    document.getElementById("errorTxt").innerText = response["error"];
                } else {
                    cookies.set("bearer_token", response["bearer_token"], {path: "/"});
                    props.logFunction(true);
                    goToConfigurator();
                }
            } else {
                document.getElementById("errorTxt").innerText = response;
            }
        });
        request.open("POST", "http://localhost:8000/login?username=" + username + "&password=" + password);
        request.send();
    }

    return (
        <div className={'LoginContainer'}>
            <h1>Login</h1>
            <form className={'LoginForm'}>
                <div className={'inputField'}>
                    <label htmlFor={'username'}>Nutzername</label>
                    <input id={'username'} type={'text'} className={'textInput'} value={username}
                           onChange={handleInputChange}/>
                </div>
                <div className={'inputField'}>
                    <label htmlFor={'password'}>Passwort</label>
                    <input id={'password'} type={'password'} className={'textInput'} value={password}
                           onChange={handleInputChange}/>
                </div>
                <div id={"errorTxt"}>
                </div>
            </form>
            <button id={'Button1'} className={'Buttons'} onClick={login}>Einloggen</button>
            <div>
                <button id={'Button2'} className={'Buttons'} onClick={goBack}>Zurück</button>
                <button id={'Button2'} className={'Buttons'} onClick={goToRegister}>Registrieren</button>
            </div>
        </div>
    )
}

export default Login