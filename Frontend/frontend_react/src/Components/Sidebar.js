import React from "react";
import '../Styles/Sidebar.css'
import Item from "./Item";
import anime from "animejs";
import Cookies from "universal-cookie";
import SearchIcon from "../Assets/search_black_24dp.svg"

class Sidebar extends React.Component {

    state = {
        searchText: null,
        toggled: true,
        numberInterface: null,
        numberHeadphones: null,
        numberDAW: null,
        numberMonitors: null
    }

    interfaceItem = {
        category: 'interface',
        id: null,
        name: null
    }

    monitorItem = {
        category: 'monitor',
        id: null,
        name: null
    }
    headphonesItem = {
        category: 'headphones',
        id: null,
        name: null
    }
    dawItem = {
        category: 'daw',
        id: null,
        name: null
    }

    items = document.getElementsByClassName('timelineStep')
    itemsSelected = [false, false, false, false]
    alreadySelected = []
    category = 'none'
    counter = 0

    componentDidMount() {
        const itemDataStorage = localStorage.getItem('itemData')
        if (itemDataStorage) {
            const itemData = JSON.parse(itemDataStorage)
            for (let i = 0; i < itemData.length; i++) {
                this.items[i].style.backgroundColor = '#4df63e'
                let type = itemData[i][0].type

                if (type === 'interface') {
                    this.setInterface(itemData[i][0].name, parseInt(localStorage.getItem('interfaceId')))
                }
                if (type === 'headphones') {
                    this.setHeadphones(itemData[i][0].name, parseInt(localStorage.getItem('headphonesId')))
                }
                if (type === 'daw') {
                    this.setDAW(itemData[i][0].name, parseInt(localStorage.getItem('dawId')))
                }
                if (type === 'monitor') {
                    this.setMonitor(itemData[i][0].name, parseInt(localStorage.getItem('monitorId')))
                }
            }
            this.itemsSelected = [true, true, true, true]
            this.alreadySelected = ['interface', 'headphones', 'monitor', 'daw']
            this.counter = 3
            if (this.itemsSelected.every(v => v === true)) this.props.changer(true)
            else {
                this.props.changer(false)
            }
        }

        anime({  //Start-Animation
            targets: this.items,
            opacity: 100,
            translateX: [-100, 0],
            delay: anime.stagger(100)
        })
    }

    showSidebar = (bool) => { //Make Sidebar Visible
        if (this.itemsSelected.every(v => v === true)) this.props.changer(true)
        else {
            this.props.changer(false)
        }
        this.category = this.props.getter()
        this.setState({
            toggled: bool
        })
    }

    //Setter-functions for Items

    setInterface = (name, id) => {
        this.interfaceItem = {
            category: 'interface',
            id: id,
            name: name

        }
    }
    setMonitor = (name, id) => {

        this.monitorItem = {
            category: 'monitor',
            id: id,
            name: name
        }
    }

    setDAW = (name, id) => {

        this.dawItem = {
            category: 'daw',
            id: id,
            name: name
        }
    }
    setHeadphones = (name, id) => {

        this.headphonesItem = {
            category: 'headphones',
            id: id,
            name: name
        }
    }

    colorItems(number) {  //Color Timeline-Items
        if (this.alreadySelected.includes(this.category)) {
            if (number > 0) {
                number = 0
            } else {
                let index = this.alreadySelected.indexOf(this.category)
                this.alreadySelected.splice(index, 1)
            }
        } else {
            this.alreadySelected.push(this.category)
        }

        if (number > 0) {
            if (this.itemsSelected[this.counter] === false) {
                this.itemsSelected[this.counter] = true
                this.items[this.counter].style.backgroundColor = 'green'
                anime({
                    targets: this.items[this.counter],
                    backgroundColor: '#4df63e',
                });
                this.counter += number
                if (this.counter >= this.items.length - 1) this.counter = 3
            }
        }
        if (number < 0) {
            if (this.itemsSelected[this.counter] === true) {
                this.itemsSelected[this.counter] = false
                this.items[this.counter].style.backgroundColor = 'red'
                if (this.counter === 3) {
                    this.counter = 3
                } else {
                    this.counter += number
                }

                if (this.counter <= 0) this.counter = 0
            } else {
                if (typeof this.items[this.counter - 1] != "undefined") {
                    this.itemsSelected[this.counter - 1] = false
                    this.items[this.counter - 1].style.backgroundColor = 'red'
                    this.counter += number

                    if (this.counter <= 0) this.counter = 0
                }
            }
        }
        this.showSidebar(true)
    }

    getSingleItem = (object) => {   //Get one Item based on Id
        let request = new XMLHttpRequest()
        request.open('GET', 'http://localhost:8000/components/' + object.category, false);

        const cookies = new Cookies()
        let token = cookies.get("bearer_token");
        if (token !== undefined) {
            request.setRequestHeader("Authorization", "Bearer " + token);
        }

        try {
            request.send()
        } catch (e) {
            console.log(e)
        }
        let response = request.responseText
        response = JSON.parse(response)
        //let min = Math.min.apply(null, response.map((v) => v.id));
        response = response.filter(({name}) => name === object.name)
        return response
    }

    getAllSelectedItems = () => {  //Get all selected Items --> Checkout
        let itemsSelectedData = [];
        itemsSelectedData.push(this.getSingleItem(this.interfaceItem))
        itemsSelectedData.push(this.getSingleItem(this.dawItem))
        itemsSelectedData.push(this.getSingleItem(this.monitorItem))
        itemsSelectedData.push(this.getSingleItem(this.headphonesItem))
        return itemsSelectedData
    }

    changeCartList = (category, item) => {
        if (this.props.itemDataApp.some(e => e.category === category)) {
            let indexOfItem = this.props.itemDataApp.findIndex(i => i.category === category);
            this.props.itemDataApp.splice(indexOfItem, 1)
            this.props.itemSetter(item)
        } else {
            this.props.itemSetter(item)
        }
    }

    getItems(category, callback) {  //Get all items in category
        let request = new XMLHttpRequest();
        request.open("GET", "http://localhost:8000/components/" + this.category, false);

        const cookies = new Cookies()
        let token = cookies.get("bearer_token");
        if (token !== undefined) {
            request.setRequestHeader("Authorization", "Bearer " + token);
        }

        try {
            request.send();
        } catch (e) {
            console.log(e);
        }
        let response = request.responseText;
        response = JSON.parse(response);

        let items = [];

        for (let i = 0; i < response.length; i++) {
            if(this.state.searchText!==null && this.state.searchText.trim().length!=0 && !response[i].name.toUpperCase().includes(this.state.searchText.toUpperCase())){
                continue
            }

            let isSet = false
            if (this.category === 'interface' && this.interfaceItem.id != null) {
                isSet = (response[i].name === this.interfaceItem.name)
            }
            if (this.category === 'headphones' && this.headphonesItem.id != null) {
                isSet = (response[i].name === this.headphonesItem.name)
            }
            if (this.category === 'daw' && this.dawItem.id != null) {
                isSet = (response[i].name === this.dawItem.name)
            }
            if (this.category === 'monitor' && this.monitorItem.id != null) {
                isSet = (response[i].name === this.monitorItem.name)
            }

            items.push(<Item class={isSet ? 'itemEntry active' : 'itemEntry'} onClick={() => {
                this.colorItems(1);
                if (this.category === 'interface') {
                    this.setInterface(response[i]["name"], i)
                    localStorage.setItem('interfaceId', i.toString())
                    this.changeCartList('interface', this.interfaceItem)
                }
                if (this.category === 'headphones') {
                    this.setHeadphones(response[i]["name"], i)
                    localStorage.setItem('headphonesId', i.toString())
                    this.changeCartList('headphones', this.headphonesItem)
                }
                if (this.category === 'daw') {
                    this.setDAW(response[i]["name"], i)
                    localStorage.setItem('dawId', i.toString())
                    this.changeCartList('daw', this.dawItem)
                }
                if (this.category === 'monitor') {
                    this.setMonitor(response[i]["name"], i)
                    localStorage.setItem('monitorId', i.toString())
                    this.changeCartList('monitor', this.monitorItem)
                }
            }} id={i} name={response[i]["name"]} price={response[i]["price"] + "$"}/>);
        }

        return React.createElement("div", {id: "sidebarItems"}, items);
    }

    render() {
        let items = null
        items = this.getItems(this.category);

        return (
            <div className={this.state.toggled ? 'sidebar' : 'sidebar active'}>
                <h1>{this.category.slice(0, 1).toUpperCase() + this.category.slice(1)}</h1>
                <div id={"searchbar"}>
                    <input id={"searchInput"} type={"text"} placeholder={"Suche"} onChange={(event) => {
                        this.setState({searchText: event.target.value})
                    }}/>
                </div>
                {items}
                <button onClick={() => this.showSidebar(true)}>Abbrechen</button>
                {this.alreadySelected.includes(this.category) ? <button onClick={() => {
                    this.colorItems(-1)
                    switch (this.category) {
                        case 'interface':
                            this.setInterface(null, null);
                            break;
                        case 'daw':
                            this.setDAW(null, null);
                            break;
                        case 'monitor':
                            this.setMonitor(null, null);
                            break;
                        case 'headphones':
                            this.setHeadphones(null, null);
                            break;
                        default:
                            console.log('No Category');
                            break;
                    }
                    if (this.props.itemDataApp.some(e => e.category === this.category)) {
                        let indexOfItem = this.props.itemDataApp.findIndex(i => i.category === this.category);
                        this.props.deleteItemData(indexOfItem)
                    }
                }}>Produkt abwählen</button> : null}
            </div>
        )
    }
}

export default Sidebar