import React from "react";
import "../Styles/SetupItem.css";
import Cookies from "universal-cookie";
import {useNavigate} from "react-router-dom";

const cookies = new Cookies();

function SetupItem(props) {
    let components = getComponents(props.setup.id);
    let displayComponents = convertToDisplayComponents(components);

    let navigate = useNavigate();

    let loadSetup = () => {
        let itemdata = [];
        let items = []
        for(let i=0; i<components.length;i++) {
            itemdata.push([components[i]]);
            let item = {category: components[i].type, name: components[i].name, id: components[i].id}
            items.push(item)
        }
        localStorage.setItem("itemData", JSON.stringify(itemdata));
        props.itemsSetter(items)

        navigate("/configurator", {state: itemdata}, {replace: true});
    }

    let onActive = (event) => {
        if(event.target.classList.contains("setupItem")) {
            if (event.target.classList.contains("active")) {
                event.target.classList.remove("active");
                event.target.lastElementChild.hidden = true;
            } else {
                event.target.classList.add("active");
                event.target.lastElementChild.hidden = false;
            }
        }
    }

    return (
      <div id={"setup" + props.setupNumber} className={"setupItem"} onClick={onActive}>
          <div className={"setupItemHeader"}>
              <h2>{"Setup " + props.setupNumber}</h2>
              <button className={"loadSetupButton"} onClick={loadSetup}>Load</button>
          </div>
          <div className={"componentsContainer"} hidden={true}>
              {displayComponents}
          </div>
      </div>
    );
}

function convertToDisplayComponents(components){
    let componentList = [];

    for(let i=0; i<components.length; i++) {
        componentList.push(
            <div key={i} className={"componentContainer"}>
                <p key={0}>{components[i].type.charAt(0).toUpperCase() + components[i].type.slice(1)}</p>
                <p key={1}>{components[i].name}</p>
                <p key={2}>{components[i].price + "$"}</p>
            </div>
        );
    }

    return componentList;
}

function getComponents(setupid){
    let request = new XMLHttpRequest()
    request.open('GET', "http://localhost:8000/setup/components/" + setupid, false);
    request.setRequestHeader("Authorization", "Bearer " + cookies.get("bearer_token"));

    try {
        request.send();
    } catch (e) {
        console.log(e);
    }

    let response = request.responseText;

    if(response.startsWith("[{")){
        return JSON.parse(response);
    }
}

export default SetupItem;