import React from 'react'
import '../Styles/ListItem.css'

function ListItem(props){
    return(
        <div  className={'listItemContainerAll'}>
            <div className={'listItemContainer'}>
                <p>{props.number}. {props.category}</p>
                <p>{props.name}</p>
                <p>{props.preis}$</p>
            </div>
            <a  href={props.link} target="_blank"><button className={'checkoutItemButton'}>Zu Thomann</button></a>
        </div>
    )
}

export default ListItem