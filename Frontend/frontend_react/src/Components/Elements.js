import React from "react";
import '../Styles/GeneralStyle.css'
import '../Styles/Elements.css'
import MonitorScreen from '../Assets/screen.svg'
import Speaker from '../Assets/monitor.svg'
import Headphones from '../Assets/headphones.svg'
import Interface from '../Assets/interface.svg'


class Elements extends React.Component {

    changeCategory = (category) => {
        this.category = category
    }

    getCategory = () => {
        return this.category
    }

    category = 'none'

    showSidebar = () => {
        this.props.sidebarToggler(false)
    }

    hideSidebar = () => {
        this.props.sidebarToggler(true)
    }

    render() {
        return (
            <div className={'itemContainer'}>
                <div className={'audioItem'} onClick={() => {
                    this.changeCategory('daw')
                    this.showSidebar()
                }}>
                    <img src={MonitorScreen} alt={'MonitorLogo'}/>
                </div>
                <div id={'secondRow'}>
                    <div className={'audioItem'} onClick={() => {
                        this.changeCategory('monitor')
                        this.showSidebar()
                    }}>
                        <img className={'speaker'} src={Speaker} alt={'SpeakerLogo'}/>
                    </div>
                    <div className={'audioItem'} onClick={() => {
                        this.changeCategory('interface')
                        this.showSidebar()
                    }}>
                        <img src={Interface} alt={'InterfaceLogo'}/>
                    </div>

                    <div className={'audioItem'} onClick={() => {
                        this.changeCategory('headphones')
                        this.showSidebar()
                    }}>
                        <img src={Headphones} alt={'HeadphoneLogo'}/>
                    </div>
                    <div className={'audioItem'} onClick={() => {
                        this.changeCategory('monitor')
                        this.showSidebar()
                    }}>
                        <img className={'speaker'} src={Speaker} alt={'SpeakerLogo'}/>
                    </div>
                </div>
            </div>

        )
    }

}

export default Elements