import React from "react";
import "../Styles/Setups.css";
import {useNavigate} from "react-router-dom";
import SetupItem from "./SetupItem";
import Cookies from "universal-cookie";

const cookies = new Cookies();

class Setup {
    id = 0;
    components = [];

    constructor(id) {
        this.id = id;
    }
}

function Setups(props) {
    let navigate = useNavigate();
    let setupList = [];

    if(props.loggedStatus){
        let setupDb = getSetups();
        if(setupDb === undefined){
            props.logFunction(false);
        } else {
            let setups = [];
            for(let i = 0; i<setupDb.length; i++){
                if(getSetup(setups, setupDb[i]["setupid"]) !== undefined){
                    getSetup(setups, setupDb[i]["setupid"]).components.push(setupDb[i]["componentid"]);
                } else {
                    let setup = new Setup(setupDb[i]["setupid"]);
                    setup.components.push(setupDb[i]["componentid"]);
                    setups.push(setup);
                }
            }
            for (let i=0; i<setups.length; i++) {
                setupList.push(<SetupItem key={i} setupNumber={i} itemsSetter={props.itemsSetter} setup={setups[i]}/>);
            }
        }
    }

    let goToLogin = () => {
        navigate('/login', {replace: true});
    }

    return (
        <div className={"setupsContainer"}>
            {props.loggedStatus ?
                <div id={"setupList"} hidden={!props.loggedStatus}>
                    {setupList}
                </div>

                :

                <div className={"loggedOffContainer"} hidden={props.loggedStatus}>
                    <p>Du musst dich einloggen um deine gespeicherten Setups anzuzeigen!</p>
                    <button id={"loginButton"} onClick={goToLogin} hidden={props.loggedStatus}>Login</button>
                </div>
            }
        </div>
    );
}

function getSetups(){
    let request = new XMLHttpRequest()
    request.open('POST', "http://localhost:8000/setup", false);
    request.setRequestHeader("Authorization", "Bearer " + cookies.get("bearer_token"));

    try {
        request.send();
    } catch (e) {
        return undefined
    }
    let response = request.responseText;

    if(response.startsWith("[")){
        return JSON.parse(response);
    } else {
        return undefined;
    }
}

function getSetup(list, id){
    for(let i=0; i<list.length; i++) {
        if(list[i].id===id){
            return list[i];
        }
    }
    return undefined;
}

export default Setups;