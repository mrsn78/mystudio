# MyStudio

Dies ist das offizielle THM-Git-Repo für das Projekt "MyStudio" für MIB33 Web-Frameworks.

## Über das Projekt
MyStudio ist unser Projekt für das Fach MIB33 Web-Frameworks.
Hierbei handelt es sich von der Grundidee um einen Konfigurator für Audio-Setups.

In mehreren Schritten soll sich der User ein für ihn perfektes Setup aus verschiedenen Kategorien von Audio-Komponenten zusammenstellen können.

### Funktionen
* Clickable Studio Setup
* "Timeline" mit Checkmarks für Komponenten
* Komponentenauswahl mit Suchleiste und Price Range
* Warenkorb
* Login
    * Setup speichern
* optional:
    * Thomann verlinkung
    * Empfohlene Setups
        * Rating
        * Kommentare
    * Produktbewertung

### Frameworks

#### Kombination 1
> Backend: [Lumen](https://lumen.laravel.com/) <br>
> Frontend: [React](https://reactjs.org/)

#### Kombination 2
> Backend: [Lumen](https://lumen.laravel.com/) <br>
> Frontend: [Svelte](https://svelte.dev/)

## Installations-Anleitung

### Backend: Lumen

Um die nötigen Dependencies im Backend zu installieren muss folgender Befehl im **Backend**-Ordner ausgeführt werden:

```
composer install
```

Zum Starten muss folgender Befehl genutzt werden:

```
php -S localhost:8000 -t public
```

#### Verknüpfung der Datenbank

Unter <a href="https://drive.google.com/drive/folders/1Pw3_Vlf2CGBDZ977BLx5OtP_OE4Sa409?usp=sharing" target="_blank">diesem Link</a> kann eine .sql-Datei mit allen nötigen Tabellen und Test-Daten heruntergeladen werden.

Zum Verknüpfen der Datenbank mit dem Backend gibt es nach dem Aufsetzen 2 Möglichkeiten:

##### 1.Möglichkeit

Die Datenbank läuft auf **mysql** unter **127.0.0.1:3306** und hat den Namen **mystudio**. Außerdem ist ein Nutzer mit ausreichenden Rechten angelegt, der den Nutzernamen **mystudio** und das Passwort **secret** besitzt.

##### 2.Möglichkeit

Wenn die Datenbank nicht genau unseren Einstellungen folgt, können diese im **Backend**-Ordner in der **.env** Datei eingetragen werden. Die folgenden Zeilen sind dafür relevant:

```
DB_CONNECTION={Name des Datenbanksystems z.B. mysql}
DB_HOST={Ip-Adresse der Datenbank}
DB_PORT={Port der Datenbank}
DB_DATABASE={Name der Datenbank/des Schemas}
DB_USERNAME={Nutzername des Datenbank Nutzers}
DB_PASSWORD={Passwort des Datenbank Nutzers}
```

### Frontend: React

Zum Installieren der Abhängigkeiten muss der folgende Befehl im **frontend_react**-Ordner ausgeführt werden:

```
npm install
```

Um den Server zu Starten reicht folgender Command aus:

```
npm start
```

### Frontend: Svelte

Zum Installieren der Abhängigkeiten muss der folgende Befehl im **frontend_svelte**-Ordner ausgeführt werden:

```
npm install
```

Um den Server zum Laufen zu bringen muss folgender Befehl ausgeführt werden:

```
npm run dev
```

**Anmerkung: Durch einen Fehler, welchen wir bisher nicht beheben konnten, wird die Svelte-Anwendung beim ersten Laden einen Fehler anzeigen.
Um dies zu beheben, einfach das Browser-Fenster neuladen.**

## Team
* Marc Rosenberger 
* Patrick Walczynski
