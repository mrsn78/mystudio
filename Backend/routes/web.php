<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () {
    return "";
});

$router->get('auth', ['middleware' => 'auth', function () { //Authentication test
    return Auth::user();
}]);

$router->post('logout', function (Request $request) {
    DB::update("UPDATE users SET last_activity =NULL, bearer_token=NULL
                WHERE username='" . $request->input("username") . "'");
});

$router->post('login', function (Request $request) { //User login
    $token = DB::select("SELECT bearer_token FROM users WHERE username='" . $request->input("username") . "' AND password='" . $request->input("password") . "'");
    if (!empty($token)) { //User is registered
        $token = $token[0];
        if (empty(str_replace('"', "", $token->bearer_token))) { //No Bearer Token generated yet
            $token = generateBearerToken();
            DB::update("UPDATE users SET bearer_token = '" . $token . "', last_activity = '" . date("Y-m-d H:i:s") . "' WHERE username = '" . $request->input("username") . "'");
            return response()->json(["bearer_token" => $token]);
        } else {
            DB::update("UPDATE users SET last_activity = '" . date("Y-m-d H:i:s") . "' WHERE username = '" . $request->input("username") . "'");
            return response()->json($token);
        }
    } else { //User is not registered or wrong Login Credentials
        return response()->json(["error" => "Falscher Nutzername oder Passwort"], 401);
    }
});

$router->post('register', function (Request $request){ //User registration
    if($request->filled(["username","password","email"])){ //Post includes all necessary fields
        if(filter_var($request->input("email"), FILTER_VALIDATE_EMAIL)){ //Email validation
            if (strlen($request->input("password")) >= 5) {
                $usertest = DB::select("SELECT * FROM users WHERE username='" . $request->input("username") . "'");
                if (empty($usertest)) { //Check if username is already in use
                    $token = generateBearerToken();
                    DB::insert("INSERT INTO users (username, password, email, bearer_token, last_activity) VALUES (?,?,?,?,?)",
                        [$request->input("username"), $request->input("password"), $request->input("email"), $token, date("Y-m-d H:i:s")]);
                    return response()->json(["bearer_token" => $token]);
                } else {
                    return response("Nutzername ist schon vergeben", 409);
                }
            } else {
                return response("Das Passwort muss mindestens 5 Zeichen lang sein", 400);
            }
        } else {
            return response("Bitte korrekte Email-Adresse eingeben", 400);
        }
    } else {
        return response("Bitte alle Felder ausfüllen", 400);
    }
});

$router->get('ping', function () { //Test Function
    return "Pong";
});

$router->get('component/{id}', function ($id) { //Component Api
    $query = "SELECT * FROM components WHERE id='".$id."'";
    return json_encode(DB::select($query)[0]);
});

$router->get('components[/{type}]', function ($type = null) { //Component Api
    $query = "SELECT * FROM components";
    if ($type != null) {
        $query .= " WHERE type='" . $type . "'";
    }
    return DB::select($query);
});

$router->post('setup[/{operation}]', ['middleware' => 'auth', function (Request $request, $operation = null) {
    $username = DB::select("SELECT username FROM users WHERE bearer_token='" . $request->bearerToken() . "'")[0]->{"username"};

    if ($operation == null) {
        return DB::select("SELECT * FROM setups WHERE username='" . $username . "'");
    } else if ($operation == "save" and $request->isJson()) {
        $content = json_decode($request->getContent(), true);
        $setupid = 0;
        $setups = DB::select("SELECT setupid FROM setups order by setupid desc limit 1");
        if (!empty($setups)) {
            $setupid = $setups[0]->{"setupid"} + 1;
        }

        if (array_key_exists("name", $content)  && strlen($content["name"])!=0) {
            $setupname = $content["name"];
        } else {
            $setupname = null;
        }
        foreach ($content["components"] as $component) {
            DB::insert("INSERT INTO setups (setupid, username, componentid, setupname) VALUES (?,?,?,?)",
                [$setupid, $username, $component, $setupname]);
        }

        return response()->json(["setupid" => $setupid]);
    }
}]);

$router->get('setup/components/{setupId}', ['middleware' => 'auth', function (Request $request, $setupId) {
    $username = DB::select("SELECT username FROM users WHERE bearer_token='" . $request->bearerToken() . "'")[0]->{"username"};

    $setup = DB::select("SELECT componentid FROM setups WHERE username='" . $username . "' AND setupid='" . $setupId . "'");

    $componentIds = array();
    $i = 0;
    foreach ($setup as $component) {
        $componentIds[$i] = $component->{"componentid"};
        $i++;
    }

    if (empty($componentIds)) {
        return response("Wrong SetupId", 400);
    }

    $query = "SELECT * FROM components WHERE id='" . $componentIds[0] . "'";

    for ($i = 1; $i < count($componentIds); $i++) {
        $query .= " OR id='" . $componentIds[$i] . "'";
    }

    $components = DB::select($query);

    return $components;

}]);

function generateBearerToken()
{
    $token = "";
    while (strlen($token) < 20) {
        $charRange = random_int(0, 2); //Determines if the character will be a number, uppercase or lower case letter
        switch ($charRange) {
            case 0: //Number
                $token .= chr(random_int(48, 57));
                break;
            case 1: //Uppercase letter
                $token .= chr(random_int(65, 90));
                break;
            case 2: //Lowercase letter
                $token .= chr(random_int(97, 122));
                break;
        }
    }
    $tokenTest = DB::select("SELECT * FROM users WHERE bearer_token='" . $token . "'");
    if (empty($tokenTest)) {
        return $token;
    } else {
        return generateBearerToken();
    }
}
