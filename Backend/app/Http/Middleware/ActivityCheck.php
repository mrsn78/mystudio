<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class ActivityCheck
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->bearerToken();
        if (!is_null($token)) {
            $last_activity = DB::select("SELECT last_activity FROM users WHERE bearer_token='" . $token . "'");
            if (empty($last_activity)) {
                return $next(request());
            }
            $lastActivityTime = date_create_from_format("Y-m-d H:i:s", $last_activity[0]->{"last_activity"});
            if ($lastActivityTime && $this->dateDiff_to_seconds(date_diff($lastActivityTime, date_create())) > (15 * 60)) {
                DB::update("UPDATE users SET last_activity =NULL, bearer_token=NULL
                WHERE bearer_token='" . $token . "'");
            } else {
                DB::update("UPDATE users SET last_activity ='" . date("Y-m-d H:i:s") . "'
                WHERE bearer_token='" . $token . "'");
            }
        }

        return $next($request);
    }

    private function dateDiff_to_seconds(\DateInterval $diff)
    {
        return ((($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i) * 60 + $diff->s;
    }
}
